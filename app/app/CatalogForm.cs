﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Windows.Forms;

namespace app
{
    public partial class CatalogForm : Form
    {

        private int privilegeLevel;
        private bool doneLoadingForm = false;

        private OleDbConnection connection;
        private string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + AppDomain.CurrentDomain.GetData("dbFile").ToString();

        public CatalogForm(int _privilege)
        {
            privilegeLevel = _privilege;
            InitializeComponent();
        }

        private void CatalogForm_Load(object sender, EventArgs e)
        {
            switch (privilegeLevel)
            {
                case 1:
                    {
                        // admin
                        dgvCatalog.Visible = true;
                        dgvSales.Visible = true;
                        btnSellItem.Visible = false;

                        loadCatalogFromDB();
                        loadSalesFromDB();

                        dgvSales.AllowUserToAddRows = false;

                        break;
                    }
                case 2:
                    {
                        // manager
                        dgvCatalog.Visible = true;
                        dgvSales.Visible = false;

                        loadCatalogFromDB();

                        dgvCatalog.AllowUserToAddRows = false;

                        break;
                    }
                default:
                    {
                        MessageBox.Show("Unknown privilege level, exiting..");
                        Application.Exit();
                        break;
                    }
            }
            doneLoadingForm = true;
        }

        private void saveCatalogToDB()
        {
            
        }

        private void saveSalesToDB()
        {

        }

        private void loadCatalogFromDB()
        {
            DataTable dt = getDBData("SELECT * FROM [catalog]");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataGridViewRow row = (DataGridViewRow) dgvCatalog.Rows[0].Clone();
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    row.Cells[j].Value = dt.Rows[i].ItemArray[j];
                }
                dgvCatalog.Rows.Add(row);
            }
        }

        private void loadSalesFromDB()
        {
            DataTable dt = getDBData("SELECT * FROM [sales]");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataGridViewRow row = (DataGridViewRow) dgvSales.Rows[0].Clone();
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    row.Cells[j].Value = dt.Rows[i].ItemArray[j];
                }
                row.Cells[6].Value = getSaleIncome(double.Parse(dt.Rows[i].ItemArray[3].ToString()), double.Parse(dt.Rows[i].ItemArray[4].ToString()));
                dgvSales.Rows.Add(row);
            }
        }

        private DataTable getDBData(String sql)
        {
            connection = new OleDbConnection(connectionString);
            DataTable table = new DataTable();
            using (OleDbDataAdapter da = new OleDbDataAdapter(sql, connection))
            {
                da.Fill(table);
            }
            return table;
        }

        private void updateCatalogQuantityField(int _quantity, int _rowId)
        {

            string query = "update [catalog] set [quantity] = ? where id = ?";
            OleDbCommand cmd = new OleDbCommand(query, connection);
            connection.Open();
            connection = new OleDbConnection(connectionString);
            cmd.Parameters.AddWithValue("quantity", _quantity);
            cmd.Parameters.AddWithValue("id", _rowId);
            
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        private double getSaleIncome(double _quantity, double _pricePerUnit)
        {
            return _quantity * _pricePerUnit;
        }

        private void dgvCatalog_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            try
            {
                MessageBox.Show("Row deleted event, TODO");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvCatalog_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            try
            {
                MessageBox.Show("Row added event, TODO");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvCatalog_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (doneLoadingForm)
            {
                if (e.ColumnIndex == 3)
                {
                    try
                    {
                        int id = e.RowIndex + 1;
                        int value = int.Parse(dgvCatalog.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                        updateCatalogQuantityField(value, id);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void dgvCatalog_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView) sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                MessageBox.Show("Sale button clicked, TODO");
            }
        }
    }
}
