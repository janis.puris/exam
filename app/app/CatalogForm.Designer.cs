﻿namespace app
{
    partial class CatalogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvCatalog = new System.Windows.Forms.DataGridView();
            this.txtCatalogItemId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCatalogItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCatalogItemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCatalogItemQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCatalogItemPricePerUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSellItem = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgvSales = new System.Windows.Forms.DataGridView();
            this.txtSalesItemId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSalesItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSalesItemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSalesItemQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSalesItemPricePerUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSalesItemCustomer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSalesItemIncome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCatalog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSales)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvCatalog
            // 
            this.dgvCatalog.AllowUserToOrderColumns = true;
            this.dgvCatalog.AllowUserToResizeRows = false;
            this.dgvCatalog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCatalog.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgvCatalog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvCatalog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCatalog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtCatalogItemId,
            this.txtCatalogItemName,
            this.txtCatalogItemType,
            this.txtCatalogItemQuantity,
            this.txtCatalogItemPricePerUnit,
            this.btnSellItem});
            this.dgvCatalog.Location = new System.Drawing.Point(12, 12);
            this.dgvCatalog.Margin = new System.Windows.Forms.Padding(12);
            this.dgvCatalog.Name = "dgvCatalog";
            this.dgvCatalog.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvCatalog.Size = new System.Drawing.Size(752, 265);
            this.dgvCatalog.TabIndex = 1;
            this.dgvCatalog.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCatalog_CellClick);
            this.dgvCatalog.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCatalog_CellValueChanged);
            this.dgvCatalog.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvCatalog_UserAddedRow);
            this.dgvCatalog.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvCatalog_UserDeletedRow);
            // 
            // txtCatalogItemId
            // 
            this.txtCatalogItemId.HeaderText = "id";
            this.txtCatalogItemId.Name = "txtCatalogItemId";
            this.txtCatalogItemId.ReadOnly = true;
            this.txtCatalogItemId.Visible = false;
            // 
            // txtCatalogItemName
            // 
            this.txtCatalogItemName.HeaderText = "Nosaukums";
            this.txtCatalogItemName.Name = "txtCatalogItemName";
            // 
            // txtCatalogItemType
            // 
            this.txtCatalogItemType.HeaderText = "Tips";
            this.txtCatalogItemType.Name = "txtCatalogItemType";
            // 
            // txtCatalogItemQuantity
            // 
            this.txtCatalogItemQuantity.HeaderText = "Daudzums";
            this.txtCatalogItemQuantity.Name = "txtCatalogItemQuantity";
            // 
            // txtCatalogItemPricePerUnit
            // 
            this.txtCatalogItemPricePerUnit.HeaderText = "Cena gabalā";
            this.txtCatalogItemPricePerUnit.Name = "txtCatalogItemPricePerUnit";
            // 
            // btnSellItem
            // 
            this.btnSellItem.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.btnSellItem.HeaderText = "Pārdot";
            this.btnSellItem.Name = "btnSellItem";
            // 
            // dgvSales
            // 
            this.dgvSales.AllowUserToOrderColumns = true;
            this.dgvSales.AllowUserToResizeRows = false;
            this.dgvSales.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSales.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgvSales.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtSalesItemId,
            this.txtSalesItemName,
            this.txtSalesItemType,
            this.txtSalesItemQuantity,
            this.txtSalesItemPricePerUnit,
            this.txtSalesItemCustomer,
            this.txtSalesItemIncome});
            this.dgvSales.Location = new System.Drawing.Point(12, 301);
            this.dgvSales.Margin = new System.Windows.Forms.Padding(12);
            this.dgvSales.Name = "dgvSales";
            this.dgvSales.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvSales.Size = new System.Drawing.Size(752, 265);
            this.dgvSales.TabIndex = 2;
            // 
            // txtSalesItemId
            // 
            this.txtSalesItemId.HeaderText = "ID";
            this.txtSalesItemId.Name = "txtSalesItemId";
            this.txtSalesItemId.ReadOnly = true;
            this.txtSalesItemId.Visible = false;
            // 
            // txtSalesItemName
            // 
            this.txtSalesItemName.HeaderText = "Nosaukums";
            this.txtSalesItemName.Name = "txtSalesItemName";
            // 
            // txtSalesItemType
            // 
            this.txtSalesItemType.HeaderText = "Tips";
            this.txtSalesItemType.Name = "txtSalesItemType";
            // 
            // txtSalesItemQuantity
            // 
            this.txtSalesItemQuantity.HeaderText = "Daudzums";
            this.txtSalesItemQuantity.Name = "txtSalesItemQuantity";
            // 
            // txtSalesItemPricePerUnit
            // 
            this.txtSalesItemPricePerUnit.HeaderText = "Cena gabalā";
            this.txtSalesItemPricePerUnit.Name = "txtSalesItemPricePerUnit";
            // 
            // txtSalesItemCustomer
            // 
            this.txtSalesItemCustomer.HeaderText = "Klients";
            this.txtSalesItemCustomer.Name = "txtSalesItemCustomer";
            // 
            // txtSalesItemIncome
            // 
            this.txtSalesItemIncome.HeaderText = "Peļņa";
            this.txtSalesItemIncome.Name = "txtSalesItemIncome";
            // 
            // CatalogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1015, 854);
            this.Controls.Add(this.dgvSales);
            this.Controls.Add(this.dgvCatalog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "CatalogForm";
            this.Text = "Virtuālā noliktava elektroprecēm";
            this.Load += new System.EventHandler(this.CatalogForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCatalog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSales)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvCatalog;
        private System.Windows.Forms.DataGridView dgvSales;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCatalogItemId;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCatalogItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCatalogItemType;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCatalogItemQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCatalogItemPricePerUnit;
        private System.Windows.Forms.DataGridViewButtonColumn btnSellItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtSalesItemId;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtSalesItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtSalesItemType;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtSalesItemQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtSalesItemPricePerUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtSalesItemCustomer;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtSalesItemIncome;
    }
}

