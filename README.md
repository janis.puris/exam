Vidzemes Augstskola
Inženierzinātņu fakultāte
Programmēšana II

# Eksāmens

Izveidot lietotni "Virtuālā noliktava elektroprecēm"
Lietotnē ir jāiekļauj
- Datu bāze [kas glabājas C:\data_base\tavs_nosaukums.mdb];
- Autorizācija [parole datu bāzē jābūt šifrētai pēc sha2];
- Jānodrošina divu veidu pieejas:
	- Administrators: Var pievienot noliktavas mantas:
		- Nosaukums
		- Tips
		- Daudzums
		- Cena par gab.
	- Redzēt kopējo sarakstu ar ievadītām mantām;
	- Redzēt pārdotās manta un to iegūto peļņu;
	- Redzēt mantas, kuras visas ir izpirktas un tās pasūtīt [lietotnes gadījumā, vnk pielikt klāt daudzumu tām, attiecīgi nodrošināt mantu labošanas funkcionalitāti].
	- Noliktavas pārzinis: Var pārdot iekārtas:
		- Var norādīt, kāds klients, ko nopirka.
	- Pārdodot iekārtas, viņam sistēma uzrāda, cik vēl mantas ir palikušas, un viņš nevar pārdot vairāk nekā tās ir reāli sistēmā uzskaitītas.